package com.qcon.logodemo;

//import android.support.v7.app.ActionBarActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import java.util.List;
import java.util.ArrayList;

import org.opencv.core.MatOfDMatch;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Point;
import org.opencv.utils.Converters;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.core.CvType;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.core.MatOfKeyPoint;

import android.view.View.OnTouchListener;

public class MainActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {
    private static final String TAG = "LogoDemo::Activity";

    private CameraBridgeViewBase mOpenCvCameraView;
    private Mat                  mRgba;
    private Mat                  mGray;
    private Mat                  mSignPositive;
    private Mat                  mSignNegative;
    private Mat                  mMaskPositive;
    private Mat                  mMaskNegative;
    private int                 currentState;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(MainActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public MainActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.java_camera_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }


    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void initDetector() {

        Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.haken);
        mSignPositive = new Mat();
        Utils.bitmapToMat(bMap, mSignPositive);
        Imgproc.resize(mSignPositive, mSignPositive, new Size(100, 100));
        mMaskPositive = createMask(mSignPositive);

        Bitmap bMapH = BitmapFactory.decodeResource(getResources(), R.drawable.kreuz);
        mSignNegative = new Mat();
        Utils.bitmapToMat(bMapH, mSignNegative);
        Imgproc.resize(mSignNegative, mSignNegative, new Size(100, 100));
        mMaskNegative = createMask(mSignNegative);

    }

    public void onCameraViewStarted(int width, int height) {
        initDetector();
    }

    public void onCameraViewStopped() {
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
     }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Mat overlayImage(Mat background, Mat foreground, Mat mask)//, Point location)
    {
        int startRow = background.rows()/2 -  foreground.rows() / 2 ;
        int endRow = background.rows()/2 + foreground.rows() / 2;
        int startCol =  background.cols()/2 - foreground.cols() / 2;
        int endCol = background.cols()/2 + foreground.cols() / 2;

        Mat submat = background.submat(startRow,endRow,startCol,endCol);
        foreground.copyTo(submat, mask);

        return background;
    }

    public Mat createMask (Mat sprite){
        Mat mCurrentMask = new Mat(sprite.width(),sprite.height(),24);
        double f[] = {1,1,1,0};
        double e[] = {0,0,0,0};
        for(int y = 0; y < (int)(sprite.rows()) ; ++y)
        {
            for(int x = 0; x < (int)(sprite.cols()) ; ++x)
            {
                double info[] = sprite.get(y, x);
                if(info[3]>0) //rude but this is what I need
                {
                    mCurrentMask.put(y, x, f);
                }
                else mCurrentMask.put(y, x, e);
            }
        }
        return mCurrentMask;
    }
    public boolean onTouch(View v, MotionEvent event) {


        Log.i(TAG, "called touch");

        currentState += 1;
        if (currentState > 3) {
            currentState=0;
        }

        switch (currentState) {
            case 1:
               Toast.makeText(getApplicationContext(), "Ihr Objekt wurde als Original erkannt.",
                        Toast.LENGTH_SHORT).show();
                break;
            case 3:
               Toast.makeText(getApplicationContext(), "Ihr Objekt wurde leider nicht als Original erkannt!",
                        Toast.LENGTH_SHORT).show();
                break;
        }

        return false;
    }
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba =  inputFrame.rgba();

        Bitmap bitmap = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(mRgba, bitmap);
        //do something with bitmap
        Utils.bitmapToMat(bitmap, mRgba);

        switch (currentState) {
            case 1:
                overlayImage(mRgba, mSignPositive, mMaskPositive);
                break;
            case 3:
                overlayImage(mRgba, mSignNegative, mMaskNegative);
                break;
        }
        return mRgba;
    }

}
